package app_test

import (
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	testlog "github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/release-cli/internal/app"
	"gitlab.com/gitlab-org/release-cli/internal/testdata"
)

func TestApp(t *testing.T) {
	tests := []struct {
		name           string
		res            int
		args           []string
		wantErrStr     string
		wantLogEntries []string
	}{
		{
			name:           "success",
			res:            testdata.ResponseSuccess,
			args:           []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "v1.1.0"},
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		{
			name:           "success_with_description_file",
			res:            testdata.ResponseSuccess,
			args:           []string{"create", "--name", "release name", "--description", "testdata/description.txt", "--tag-name", "v1.1.0"},
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		{
			name:           "success_with_description_single_word",
			res:            testdata.ResponseSuccess,
			args:           []string{"create", "--name", "release name", "--description", "description", "--tag-name", "v1.1.0"},
			wantLogEntries: []string{"Creating Release...", "file does not exist, using string", "release created successfully!"},
		},
		{
			name: "success_with_assets",
			res:  testdata.ResponseSuccess,
			args: []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "v1.1.0",
				"--assets-links-name", "asset name 1", "--assets-links-url", "assets url 1"},
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		{
			name: "failed_with_uneven_assets",
			res:  testdata.ResponseSuccess,
			args: []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "v1.1.0",
				"--assets-links-name", "asset name 1", "--assets-links-name", "assets name 2"},
			wantErrStr:     "new CreateReleaseRequest: failed to parse assets: mismatch length --assets-links-name (2) and --assets-links-url (0) should be equal",
			wantLogEntries: []string{"Creating Release..."},
		},
		{
			name: "success_with_assets_link_json",
			res:  testdata.ResponseSuccess,
			args: []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "v1.1.0",
				"--assets-link", `{"name":"Asset1","url":"https://<domain>/some/location/1","link_type":"other","filepath":"xzy1"}`},
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		{
			name: "failed_with_assets_not_json",
			res:  testdata.ResponseSuccess,
			args: []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "v1.1.0",
				"--assets-link", "not_json"},
			wantErrStr:     "new CreateReleaseRequest: failed to parse assets: invalid JSON: \"not_json\"",
			wantLogEntries: []string{"Creating Release..."},
		},
		{
			name: "success_with_milestones_and_released_at",
			res:  testdata.ResponseSuccess,
			args: []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "v1.1.0",
				"--assets-links-name", "asset name 1", "--assets-links-url", "url 1",
				"--milestone", "v1.0", "--milestone", "v1.0-rc", "--released-at", "2019-01-03T01:55:18.203Z"},
			wantErrStr:     "",
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		{
			name: "failed_with_unknown_milestone",
			res:  testdata.ResponseMilestoneNotFound,
			args: []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "v1.1.0",
				"--milestone", "unknown"},
			wantErrStr:     "failed to create release: API Error Response status_code: 400 message: Milestone(s) not found: unknown",
			wantLogEntries: []string{"Creating Release..."},
		},
		{
			name: "failed_with_invalid_released_at",
			res:  testdata.ResponseSuccess,
			args: []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "v1.1.0",
				"--released-at", "2019/01/03"},
			wantErrStr:     "new CreateReleaseRequest: failed to parse released-at: parsing time \"2019/01/03\" as \"2006-01-02T15:04:05Z07:00\": cannot parse \"/01/03\" as \"-\"",
			wantLogEntries: []string{"Creating Release..."},
		},
		{
			name:           "invalid_tag",
			res:            testdata.ResponseBadRequest,
			args:           []string{"create", "--name", "release name", "--description", "release description", "--tag-name", "%invalid%_char-!ters"},
			wantErrStr:     "failed to create release: API Error Response status_code: 400 message: tag_name is missing",
			wantLogEntries: []string{"Creating Release..."},
		},
		{
			name:           "unauthorized",
			res:            testdata.ResponseUnauthorized,
			args:           []string{"create", "--name", "release name", "--description", "release description", "--tag-name", t.Name(), "--ref", t.Name()},
			wantErrStr:     "failed to create release: API Error Response status_code: 401 message: 401 Unauthorized",
			wantLogEntries: []string{"Creating Release..."},
		},
		{
			name:           "forbidden",
			res:            testdata.ResponseForbidden,
			args:           []string{"create", "--name", "release name", "--description", "", "--tag-name", t.Name(), "--ref", "12345"},
			wantErrStr:     "failed to create release: API Error Response status_code: 403 message: 403 Forbidden",
			wantLogEntries: []string{"Creating Release..."},
		},
		{
			name:           "internal_server_error",
			res:            testdata.ResponseInternalError,
			args:           []string{"create", "--name", "release name", "--description", "release description", "--tag-name", t.Name(), "--ref", t.Name()},
			wantErrStr:     "failed to create release: API Error Response status_code: 500 message: 500 Internal Server Error",
			wantLogEntries: []string{"Creating Release..."},
		},
		{
			name:           "unexpected_error",
			res:            testdata.ResponseUnexpectedError,
			args:           []string{"create", "--name", "release name", "--description", "release description", "--tag-name", t.Name(), "--ref", t.Name()},
			wantErrStr:     "failed to create release: API Error Response status_code: 500 message:  error: Something went wrong",
			wantLogEntries: []string{"Creating Release..."},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			log, hook := testlog.NewNullLogger()

			s := httptest.NewServer(handler(t, tt.res))
			defer s.Close()

			testApp := app.New(logrus.NewEntry(log), tt.name)
			args := []string{"release-cli", "--server-url", s.URL, "--job-token", "token", "--project-id", "projectID"}
			args = append(args, tt.args...)
			err := testApp.Run(args)
			if tt.wantErrStr != "" {
				require.EqualError(t, err, tt.wantErrStr)
			} else {
				require.NoError(t, err)
			}
			assert.ElementsMatch(t, toStrSlice(t, hook.AllEntries()), tt.wantLogEntries)
		})
	}
}

func TestClientTimeout(t *testing.T) {
	log, hook := testlog.NewNullLogger()
	timeout := 1 * time.Millisecond

	s := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			time.Sleep(timeout * 3)
			w.WriteHeader(http.StatusNoContent)
		}))
	defer s.Close()

	testApp := app.New(logrus.NewEntry(log), t.Name())
	args := []string{"release-cli", "--server-url", s.URL, "--job-token",
		"token", "--project-id", "projectID", "--timeout", timeout.String(),
		"create", "--name", "release name", "--description",
		"release description", "--tag-name", "v1.1.0",
	}

	err := testApp.Run(args)
	require.Error(t, err)

	var urlErr *url.Error

	require.True(t, errors.As(err, &urlErr), "err must be url.Error")
	require.True(t, urlErr.Timeout(), "err must be a timeout")
	assert.ElementsMatch(t, toStrSlice(t, hook.AllEntries()), []string{"Creating Release..."})
}

func toStrSlice(t *testing.T, entries []*logrus.Entry) []string {
	t.Helper()

	all := make([]string, len(entries))
	for k, e := range entries {
		all[k] = e.Message
	}

	return all
}

func handler(t *testing.T, i int) http.HandlerFunc {
	t.Helper()

	return func(w http.ResponseWriter, r *http.Request) {
		res := testdata.Responses[i]()
		defer require.NoError(t, res.Body.Close())

		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err)

		w.Header().Set("content-type", "application/json")
		w.WriteHeader(res.StatusCode)
		_, err = w.Write(body)
		require.NoError(t, err)
	}
}
